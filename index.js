import { InfluxDB } from '@influxdata/influxdb-client';

const token =
  'iE3nNOxFtm3XHKp_PBYdgGELsbY_LUNxZP02a9L2FQNhA924sQghNMpQX5Cv98A5f8dR1_Hb9IRU8HfFZM2NaA==';
const org = 'metroclick';
const bucket = 'events';

const influxDB = new InfluxDB({
  url: 'http://localhost:8086',
  token,
});

const queryApi = influxDB.getQueryApi(org);

const query = `from(bucket: "${bucket}") 
  |> range(start: -24h)
  |> filter(fn: (r) => r._measurement == "device:info" and r.deviceName == "device1")`;
console.clear();

let i = 0;

console.time();
queryApi.queryRows(query, {
  next(row, tableMeta) {
    const o = tableMeta.toObject(row);
    console.log(
      `${o._time} ${o._measurement} from ${o.path}>${o.deviceName} to ${o.target} by ${o.userId}`,
    );
    i++;
  },
  error(err) {
    console.error(err);
  },
  complete() {
    console.log(`Found ${i} logs!`);
    console.timeEnd();
  },
});

queryApi
  .collectRows(
    `${query}
  |> count()
  |> yield(name: "count")`,
  )
  .then((res) => {
    const total = res.reduce((acc, r) => acc + r._value, 0);
    console.log(`Queried ${total} logs!`);
  })
  .catch((err) => {
    console.error(err);
  });
