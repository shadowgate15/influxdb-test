import { InfluxDB, Point } from '@influxdata/influxdb-client';
import Chance from 'chance';
import delay from 'delay';
import ProgressBar from 'progress';

const chance = new Chance('arandomseed');

const DEVICE = [
  { id: 'd1', name: 'device1', path: '>Gen>test>place' },
  { id: 'd2', name: 'device2', path: '>Gen>test>place' },
  { id: 'd3', name: 'device3', path: '>Gen>crag' },
];

const USER = [
  { id: 'user1', email: 'user1@example.com' },
  { id: 'user2', email: 'user2@example.com' },
  { id: 'user3', email: 'user3@example.com' },
];

const EVENT = [
  'device:offline',
  'get:device',
  'query:devices',
  'send:settings',
  'device:info',
];

const token =
  'iE3nNOxFtm3XHKp_PBYdgGELsbY_LUNxZP02a9L2FQNhA924sQghNMpQX5Cv98A5f8dR1_Hb9IRU8HfFZM2NaA==';
const org = 'metroclick';
const bucket = 'events';
const numOfLoops = 10_000;

const influxDB = new InfluxDB({
  url: 'http://localhost:8086',
  token,
});
const writer = influxDB.getWriteApi(org, bucket);
const bar = new ProgressBar('(:bar) :percent :etas', numOfLoops);

(async () => {
  try {
    console.clear();

    for (let i = 0; i < numOfLoops; i++) {
      const event = chance.pickone(EVENT);
      const device = chance.pickone(DEVICE);
      const user = chance.pickone(USER);
      const target = chance.pickone(['deviceHub', 'manager']);

      const point = new Point(event)
        .tag('deviceId', device.id)
        .tag('deviceName', device.name)
        .tag('path', device.path)
        .tag('userId', user.id)
        .tag('userEmail', user.email)
        .tag('target', target)
        .stringField('data', chance.string());

      writer.writePoint(point);

      bar.tick();

      await delay(chance.integer({ min: 0, max: 5 * 1000 }));
    }

    await writer.close();

    console.log('closed');
  } catch (err) {
    console.error(err);
  }
})();
